# gitlab-runner-private-registry-fix

This project is intended to patch the `config.toml` file used by Gitlab Runners,
to include authentication to a Docker private container registry at the runner level.

> If you want to use private registries as a source of images for your builds, you can set the authorization 
> configuration in the `DOCKER_AUTH_CONFIG` [variable](https://docs.gitlab.com/ee/ci/variables/#variables). 

We have our runners provisioned via automation and need to make this modification programmatically,
which presents a challenge because the edit to the TOML file is very specific and strict.

> To add `DOCKER_AUTH_CONFIG` to a Runner:
> 
> 1. Modify the Runner’s config.toml file as follows:
>    ```toml
>    [[runners]]
>      environment = ["DOCKER_AUTH_CONFIG={\"auths\":{\"registry.example.com:5000\":{\"auth\":\"${base64-encoded-username:password}\"}}}"]
>    ```
> 1. Restart the Runner service.
> 
> :white_check_mark: Note: The double quotes included in the `DOCKER_AUTH_CONFIG` data must be escaped with backslashes. This prevents them from being interpreted as TOML.
>
> :white_check_mark: Note: The environment option is a list. So your Runner may have existing entries and you should add this to the list, not replace it.

Note I have scrubbed the value that should be replaced above with ${base64-encoded-username:password}.  The _actual_ 
value should be a a base64-encoded version of `${username}:${password}` used to login to your private registry.

Gitlab's documentation on how to do this is as follows:

> ```bash
> # Note the use of "-n" - it prevents encoding a newline in the password.
> echo -n "my_username:my_password" | base64
> 
> # Example output to copy
> bXlfdXNlcm5hbWU6bXlfcGFzc3dvcmQ=
> ```

It should be stored and obtained from a secure credential store such as AWS Secrets Manager.
 
## Using the program

The program will search for all `[[runners]]` defined, and then add or append to the `environment` value from the string 
mentioned above and replace it with the appropriate value for your environment.

### Inputs

| Name                 | Type                 | Example                         |
| ---------------------|:--------------------:| -------------------------------:|
|  `$DOCKER_REGISTRY`  | Environment Variable | registry.example.com:5000       |
| `$DOCKER_AUTH_BASE64`| Environment Variable | bXlfdXNlcm5hbWU6bXlfcGFzc3dvcmQ=|

The program will look under the environment variables `$DOCKER_REGISTRY` and `$DOCKER_AUTH_BASE64` to compute actual 
value to use as the `DOCKER_AUTH_CONFIG` string above.

It is important to set the `$DOCKER_AUTH_BASE64` value directly before executing, and unset it directly after as a best 
practice.

### Behavior

The program will also skip an existing value if it finds one present, and leave intact any existing keys that may be 
defined.

It currently will only update the `environment` for runners that contain the string "docker" in the `executor` field 
of the config.toml file.
 
### Output

The program updates in place the file `/etc/gitlab-runner/config.toml` per the above directives.

## Limitations

* Not sure yet what user this should be run as in order to properly edit the file so that the Gitlab runner can use it.
* Arguments are not used so as to prevent the typing, storing or logging of the credential.
* The current intended usage is to retrieve the value from Secrets Manager for use in a CloudFormation template 
`UserData` section that bootstraps the runner.

```bash
yum install -y awscli jq
aws --region ${AWS::Region} secretsmanager get-secret-value --secret-id !Ref ReadOnlyUserCredentials --query SecretString --output text | jq -r .password > password
```

## Making Changes to This Project

There is a test file provided in the `sample` directory, and you can execute in test mode by running with an environment 
variable `$TEST_MODE` when you execute, set to any value other than an empty string (e.g. 1 or true).

When you run in this mode your changes will not overwrite the `sample/config.toml` but will produce a separate file 
`sample/output.toml`.

## Credits

The project uses [viper](https://github.com/spf13/viper) to parse and update the 
[TOML](https://github.com/toml-lang/toml) file.  The `.gitlab-ci.yml` and `build-all.sh` script were inspired 
by [this gist](https://gist.github.com/mshafiee/5a681bbefda8f26f1f257d62f5e4a699)

## More Gitlab Documentation and Notes

The steps performed by the Runner can be summed up to:

1. The registry name is found from the image name.
1. If the value is not empty, the executor will search for the authentication configuration for this registry.
1. Finally, if an authentication corresponding to the specified registry is found, subsequent pulls will make use of it.

* [Using a private container registry](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#using-a-private-container-registry)
* [Using Docker images documentation](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#define-an-image-from-a-private-container-registry)