package main

import (
	"fmt"
	"github.com/spf13/viper"
	"log"
	"os"
	"strings"
)

type RunOptions struct {
	ConfigPath        string
	WriteConfigAs     string
	DockerRegistryUrl string
	DockerAuthBase64  string
}

func setupEnvironment() RunOptions {

	var configPath = "/etc/gitlab-runner"
	var writeConfigAs = "/etc/gitlab-runner/config.toml"

	testMode := os.Getenv("TEST_MODE")
	if testMode != "" {
		configPath = "./sample"
		writeConfigAs = "./sample/output.toml"
		_ = os.Setenv("DOCKER_REGISTRY", "registry.example.com:5000")
		_ = os.Setenv("DOCKER_AUTH_BASE64", "superSecretValue")
	}

	dockerRegistryUrl := os.Getenv("DOCKER_REGISTRY")
	if dockerRegistryUrl == "" {
		log.Fatalln("required environment variable DOCKER_REGISTRY not found")
	}

	dockerAuthBase64 := os.Getenv("DOCKER_AUTH_BASE64")
	if dockerAuthBase64 == "" {
		log.Fatalln("required environment variable DOCKER_AUTH_BASE64 not found")
	}

	return RunOptions{configPath, writeConfigAs, dockerRegistryUrl, dockerAuthBase64}
}

func main() {

	options := setupEnvironment()

	loadConfig(options.ConfigPath)

	runners := viper.Get("runners")

	for _, runnerMap := range runners.([]interface{}) {
		runner := runnerMap.(map[string]interface{})
		if runner["executor"] != nil && runner["executor"].(string) != "" && caseInsensitiveContains(runner["executor"].(string), "docker") {
			if runner["environment"] == nil {
				runner["environment"] = []string{}
			}

			elements, _ := runner["environment"].([]interface{}) // type assertion and ignore

			var alreadyPresent = false
			for _, element := range elements {
				if strings.HasPrefix(element.(string), "DOCKER_AUTH_CONFIG") {
					alreadyPresent = true
				}
			}
			if !alreadyPresent {
				dockerAuthConfigString := fmt.Sprintf("DOCKER_AUTH_CONFIG={\"auths\":{\"%s\":{\"auth\":\"%s\"}}}", options.DockerRegistryUrl, options.DockerAuthBase64)
				runner["environment"] = append(elements, dockerAuthConfigString)
			}

		}
	}

	err := viper.WriteConfigAs(options.WriteConfigAs) // /etc/gitlab-runner/config.toml or ./sample/output.toml if testing
	if err != nil {
		log.Fatalln(fmt.Errorf("error writing config.toml file: %s", err))
	}

}

func loadConfig(configPath string) {
	viper.SetConfigName("config.toml")
	viper.SetConfigType("toml")
	viper.AddConfigPath(configPath)  // /etc/gitlab-runner or ./sample if testing
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalln(fmt.Errorf("error finding config.toml file: %s", err))
	}
}

// CaseInsensitiveContains in string
func caseInsensitiveContains(s, substr string) bool {
	s, substr = strings.ToUpper(s), strings.ToUpper(substr)
	return strings.Contains(s, substr)
}
